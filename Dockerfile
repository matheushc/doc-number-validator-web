FROM node:12.22

ADD . /doc-number-validator-web

WORKDIR /doc-number-validator-web

COPY package*json ./

RUN npm install

COPY . ./

EXPOSE 3000

CMD ["npm", "start"]