import { get, validate, del, update, insert } from '../api/api'

export const DocType = {
    CPF: 0,
    CNPJ: 1,
    NONE: 2,
}

export const DocTypeString = {
    CPF: 'CPF',
    CNPJ: 'CNPJ',
    NONE: 'Invalid'
}

export const mask = value => {
    if (value.length === 11) {
        return value
        .replace(/\D/g, '')
        .replace(/(\d{3})(\d)/, '$1.$2')
        .replace(/(\d{3})(\d)/, '$1.$2')
        .replace(/(\d{3})(\d{1,2})/, '$1-$2')
        .replace(/(-\d{2})\d+?$/, '$1')
    } else if (value.length === 14) {
        return value
        .replace(/\D+/g, '')
        .replace(/(\d{2})(\d)/, '$1.$2')
        .replace(/(\d{3})(\d)/, '$1.$2')
        .replace(/(\d{3})(\d)/, '$1/$2')
        .replace(/(\d{4})(\d)/, '$1-$2')
        .replace(/(-\d{2})\d+?$/, '$1')
    } else {
        return value
    }
}

export async function getDocuments(setDocuments) {
    setDocuments(await get())
}

export async function validateDocument(number) {
    return await validate(number)
}

export async function deleteDocument(number) {
    return await del(number)
}

export async function updateDocument(data) {
    return await update(data)
}

export async function insertDocument(data) {
    return await insert(data)
}
