import React, { useEffect, useState } from "react";
import { DocTypeString, DocType, mask, deleteDocument, updateDocument } from "../resource/document";
import './doc-number-validator-table.css'
import { toast } from 'react-toastify'
import Modal from './doc-number-validator-modal'
import TrashIcon from "./icons/trash-icon";
import LockIcon from "./icons/lock-icon";
import UnlockIcon from "./icons/unlock-icon";
import FilterIcon from "./icons/filter-icon";
import SortUpIcon from "./icons/sort-up-icon";
import SortDownIcon from "./icons/sort-down-icon";

export const byNumberOrder = {
   NONE: 0,
   ASC: 1,
   DESC: 2,
}

export const byTypeOrder = {
   NONE: 0,
   ASC: 1,
   DESC: 2,
}

export const byBlockOrder = {
   NONE: 0,
   ASC: 1,
   DESC: 2,
}

export const blockedType = {
   NO: 0,
   YES: 1,
   NONE: 2,
}

export default function DocNumberValidatorTable({data, setHasChange}){

   const [orderedByNumber, setOrderedByNumber] = useState(byNumberOrder.NONE)
   const [orderedByType, setOrderedByType] = useState(byTypeOrder.NONE)
   const [orderedByBlock, setOrderedByBlock] = useState(byBlockOrder.NONE)
   const [numberFilter, setNumberFilter] = useState('')
   const [typeFilter, setTypeFilter] = useState(null)
   const [blockFilter, setBlockFilter] = useState(null)
   const [showModal, setShowModal] = useState(false)
   const [action, setAction] = useState('')
   const [selectedDoc, setSelectedDoc] = useState(null)
   const [tableData, setTableData] = useState([])

   useEffect(() => {
      setTableData(data)
      // eslint-disable-next-line
   }, [data])

   useEffect(() => {
      filterByNumber()
      // eslint-disable-next-line
   }, [numberFilter])

   useEffect(() => {
      filterByType()
      // eslint-disable-next-line
   }, [typeFilter])

   useEffect(() => {
      filterByBlock()
      // eslint-disable-next-line
   }, [blockFilter])

   const continueModal = async (confirm) => {
      if (confirm) {
         if (action === 'delete') {
            await deleteDoc()
         } else {
            await blockUnblockDoc()
         }
      }

      setSelectedDoc(null)
      setAction('')
      setShowModal(false)
   }

   const deleteDocConfirm = async (doc) => {
      setAction('delete')
      setSelectedDoc(doc)
      setShowModal(true)
   }

   const deleteDoc = async () => {
      let resp = await deleteDocument(selectedDoc.number)
      if (resp === 200) {
         toast.success('Document number is successfully deleted!')
      } else {
         toast.error('An error has occurred. Try again.')
      }
      setHasChange(true)
   }

   const blockUnblockDocConfirm = async (doc) => {
      setAction(doc.isBlocked ? 'unblock' : 'block')
      setSelectedDoc(doc)
      setShowModal(true)
   }

   const blockUnblockDoc = async () => {
      let resp = await updateDocument({...selectedDoc, isBlocked: !selectedDoc.isBlocked})
      if (resp === 200) {
         toast.success(`Document number is successfully ${selectedDoc.isBlocked ? 'unblocked' : 'blocked'}!`)
      } else {
         toast.error('An error has occurred. Try again.')
      }
      setHasChange(true)
   }

   const sortByNumber = () => {
      setOrderedByType(byTypeOrder.NONE)
      setOrderedByBlock(byBlockOrder.NONE)
      if (orderedByNumber !== byNumberOrder.ASC) {
         setTableData(data.sort((a, b) => (a.number > b.number) ? 1 : -1))
         setOrderedByNumber(byNumberOrder.ASC)
      } else {
         setTableData(data.sort((a, b) => (a.number > b.number) ? -1 : 1))
         setOrderedByNumber(byNumberOrder.DESC)
      }
   }

   const sortByType = () => {
      setOrderedByNumber(byNumberOrder.NONE)
      setOrderedByBlock(byBlockOrder.NONE)
      if (orderedByType !== byTypeOrder.ASC) {
         setTableData(data.sort((a, b) => (a.docType > b.docType) ? 1 : -1))
         setOrderedByType(byTypeOrder.ASC)
      } else {
         setTableData(data.sort((a, b) => (a.docType > b.docType) ? -1 : 1))
         setOrderedByType(byTypeOrder.DESC)
      }
   }

   const sortByBlock = () => {
      setOrderedByNumber(byNumberOrder.NONE)
      setOrderedByType(byTypeOrder.NONE)
      if (orderedByBlock !== byBlockOrder.ASC) {
         setTableData(data.sort((a, b) => (a.isBlocked > b.isBlocked) ? 1 : -1))
         setOrderedByBlock(byBlockOrder.ASC)
      } else {
         setTableData(data.sort((a, b) => (a.isBlocked > b.isBlocked) ? -1 : 1))
         setOrderedByBlock(byBlockOrder.DESC)
      }
   }

   const filterByNumber = () => {
      setTableData(data.filter(doc => doc.number.startsWith(numberFilter)))
   }

   const filterByType = () => {
      if (typeFilter === DocType.CPF || typeFilter === DocType.CNPJ) {
         setTableData(data.filter(doc => parseInt(doc.docType) === typeFilter))
      } else {
         setTableData(data)
      }
   }

   const filterByBlock = () => {
      if (blockFilter === blockedType.YES || blockFilter === blockedType.NO) {
         setTableData(data.filter(doc => {
            return (doc.isBlocked ? 1 : 0) === blockFilter
         }))
      } else {
         setTableData(data)
      }
   }

   const modalContent = (action) => {
      return <div>
         <div>Warning!</div>
         <br/>
         <div>
            Are you sure to {action} this document?
         </div>
      </div>
   }

   function renderTableHeader() {
      let header = ["Number", "Type", "Blocked", "Actions"]
      return header.map((key, index) => {
         if (index === 0)
            return <th id='order-by' key={index} onClick={sortByNumber}>
               <div>
                  {key} {orderedByNumber === byNumberOrder.ASC ? <SortDownIcon size='16'></SortDownIcon> :
                  orderedByNumber === byNumberOrder.DESC ? <SortUpIcon size='16'></SortUpIcon> :
                  <FilterIcon size='16'></FilterIcon>}
               </div>
            </th>
         if (index === 1)
            return <th id='order-by' key={index} onClick={sortByType}>
               <div>
                  {key} {orderedByType === byTypeOrder.ASC ? <SortDownIcon size='16'></SortDownIcon> :
                  orderedByType === byTypeOrder.DESC ? <SortUpIcon size='16'></SortUpIcon> :
                  <FilterIcon size='16'></FilterIcon>}
               </div>
            </th>
         if (index === 2)
            return <th id='order-by' key={index} onClick={sortByBlock}>
               <div>
                  {key} {orderedByBlock === byBlockOrder.ASC ? <SortDownIcon size='16'></SortDownIcon> :
                  orderedByBlock === byBlockOrder.DESC ? <SortUpIcon size='16'></SortUpIcon> :
                  <FilterIcon size='16'></FilterIcon>}
               </div>
            </th>
         return <th key={index}>{key}</th>
      })
   }

   function renderTableFitlers() {
      let header = ["number", "type", "block", "action"]
      return header.map((key, index) => {
         if (index === 0)
            return <td id='filter-by-number' key={index} >{
               <input id='table-input'
                  placeholder='Filter by document number...'
                  value={numberFilter}
                  type="text"
                  name="doc-number"
                  maxLength="18"
                  autoComplete="off"
                  onInput={(e) => setNumberFilter(e.target.value.replace(/\D/g, ''))}
               ></input>
               }</td>
         if (index === 1)
            return <td id='filter-by-type' key={index}>
               <select id='doc-type' onChange={(e) => setTypeFilter(parseInt(e.target.value))}>
                  <option key='none' id='none' value={DocType.NONE}>Empty</option>
                  <option key='cpf' id='cpf' value={DocType.CPF}>{DocTypeString.CPF}</option>
                  <option key='cnpj' id='cnpj' value={DocType.CNPJ}>{DocTypeString.CNPJ}</option>
               </select>
            </td>
         if (index === 2)
            return <td id='filter-by-block' key={index}>
               <select id='doc-is-blocked' onChange={(e) => setBlockFilter(parseInt(e.target.value))}>
                  <option key='none' id='none' value={blockedType.NONE}>Empty</option>
                  <option key='yes' id='yes' value={blockedType.YES}>Blocked</option>
                  <option key='no' id='no' value={blockedType.NO}>Unblocked</option>
               </select>
            </td>
         return <td id='no-filter' key={index}></td>
      })
   }

   function renderTableData() {
      return tableData.map((docNum, index) => {
         const { number, docType, isBlocked } = docNum
         return (
            <tr key={index}>
               <td>{mask(number)}</td>
               <td>{docType === String(DocType.CPF) ? DocTypeString.CPF : DocTypeString.CNPJ}</td>
               <td>{
                  <input
                     type='checkbox'
                     checked={isBlocked}
                     disabled={true}
                  ></input>
               }</td>
               <td id='actions'>
                  <span id='action' onClick={() => deleteDocConfirm(docNum)}>
                     <TrashIcon size='16'></TrashIcon>
                  </span>
                  <span id='action' onClick={() => blockUnblockDocConfirm(docNum)}>
                     {docNum['isBlocked'] ?
                        <UnlockIcon size='16'></UnlockIcon> :
                        <LockIcon size='16'></LockIcon>}
                  </span>
               </td>
            </tr>
         )
      })
   }
  
   function render() {
      return (
         <div>
            {showModal ? <Modal open={true} content={modalContent(action)} onConfirm={continueModal}></Modal> : null}
            <h1 id='title'>Documents</h1>
            <table id='numbers'>
               <tbody>
                  <tr>{renderTableHeader()}</tr>
                  <tr>{renderTableFitlers()}</tr>
                  {data.length > 0 ? renderTableData() : null}
               </tbody>
            </table>
         </div>
      )
   }

   return render()
}