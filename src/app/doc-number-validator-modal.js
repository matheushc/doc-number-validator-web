import React from 'react'
import './doc-number-validator-modal.css'

export default function Modal({content, onConfirm}) {
    return <div className='modal'>
        <div className='modal-overlay'>
            <div className='modal-content'>
                {content}
                <div className='modal-buttons'>
                    <button className='modal-button' onClick={() => onConfirm(true)}>Yes, please</button>
                    <button className='modal-button' onClick={() => onConfirm(false)}>No</button>
                </div>
            </div>
        </div>
    </div>
}