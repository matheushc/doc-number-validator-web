import React from "react";
import './document-type.css'
import { DocType } from "../resource/document";


export default function DocumentType({type}){

    return <div className='document-type'>
        <div className={`document-type-item ${type === DocType.CPF ? 'selected': ''}`}>CPF</div>
        <div className={`document-type-item ${type === DocType.CNPJ ? 'selected': ''}`}>CNPJ</div>
        <div className={`document-type-item ${type === DocType.NONE ? 'error': ''}`}>Invalid</div>
    </div>
}