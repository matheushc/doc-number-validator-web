import React, { useEffect, useState } from "react";
import { getDocuments } from "../resource/document";
import DocNumberValidatorForm from "./doc-number-validator-form";
import DocNumberValidatorTable from "./doc-number-validator-table";

export default function DocNumberValidatorPage(){

    const [documents, setDocuments] = useState([])
    const [saved, setSaved] = useState(false)
    useEffect(() => {
        getDocuments(setDocuments)

        // eslint-disable-next-line
    }, [])

    useEffect(() => {
        if (saved) {
            getDocuments(setDocuments)
            setSaved(false)
        }
        // eslint-disable-next-line
    }, [saved])

    return <div>
        <h1>Document Number Validator</h1>
        <DocNumberValidatorForm setSaved={setSaved}/>
        <DocNumberValidatorTable data={documents} setHasChange={setSaved}/>
    </div>
}