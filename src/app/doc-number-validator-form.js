import React, { useEffect, useRef, useState } from "react";
import { validateDocument, insertDocument } from "../resource/document";
import './doc-number-validator-form.css'
import { DocType } from "../resource/document";
import DocumentType from "./document-type";
import { toast } from 'react-toastify'

export default function DocNumberValidatorForm({setSaved}){

    const [number, setNumber] = useState('')
    const [type, setType] = useState(false)
    const [isBlocked, setIsBlocked] = useState(false)
    const sendTimeout = useRef(null)

    useEffect(() => {
        if (!number)
            return

        if (sendTimeout.current)
            clearTimeout(sendTimeout.current)

        sendTimeout.current = setTimeout(validate, 300)
        // eslint-disable-next-line
    }, [number])

    const validate = async () => {
        let s = await validateDocument(number)
        setType(s.data.docType)
    }

    const save = async () => {
        if (!number || type === DocType.NONE)
            return
        let data = {'number': number, 'isBlocked': isBlocked}
        let respCode = await insertDocument(data)
        if (respCode === 200) {
            toast.success('Document number inserted successfully!')
            setNumber('')
            setType(false)
            setIsBlocked(false)
            setSaved(true)
        } else if (respCode === 409) {
            toast.error('Document number already exists!')
        } else {
            toast.error('An error has occurred. Try again.')
        }
    }

    const changeCheckboxValue = async () => {
        setIsBlocked(!isBlocked)
    }

    const disableButton = () => {
        return type === DocType.NONE || number.length === 0
    }

    return <form>
        <div>
            <label id='form-label'>Document Number:</label>
            <input id='form-input'
                placeholder='Insert a document number...'
                value={number}
                type="text"
                name="doc-number"
                maxLength="18"
                autoComplete="off"
                onInput={(e) => setNumber(e.target.value.replace(/\D/g, ''))}
            ></input>
            <input id='form-checkbox-input'
                type='checkbox'
                checked={isBlocked}
                onChange={changeCheckboxValue}
            ></input>
            <label id='form-checkbox-label'>Block</label>
        </div>
        <DocumentType type={type}></DocumentType>
        <button id='btn' type='button' onClick={save} disabled={disableButton()}>
                Save
        </button>
    </form>
}