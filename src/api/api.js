import axios from 'axios'

const api = axios.create({
    baseURL: process.env.REACT_APP_API_URL
})

export async function getStatus() {
    try{
        const resp = await api.get('/status')
        return resp.data
    } catch (err) {
        return err.response.status
    }
}

export async function get() {
    try{
        const resp = await api.get('/all')
        return resp.data
    } catch (err) {
        return err.response.status
    }
}

export async function validate(number) {
    try{
        const resp = await api.get('/validate/' + number)
        return resp
    } catch (err) {
        return err.response.status
    }
}

export async function del(number) {
    try{
        const resp = await api.delete('delete/' + number)
        return resp.status
    } catch (err) {
        return err.response.status
    }
}

export async function update(data) {
    try{
        const resp = await api.put('update', data)
        return resp.status
    } catch (err) {
        return err.response.status
    }
}

export async function insert(data) {
    try{
        const resp = await api.post('add', data)
        return resp.status
    } catch (err) {
        return err.response.status
    }
}
