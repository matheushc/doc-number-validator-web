import './App.css';
import DocNumberValidatorPage from './app/doc-number-validator-page'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <div className="App">
      <DocNumberValidatorPage/>
      <ToastContainer/>
    </div>
  );
}

export default App;
